## ECXX Asteroids

Asteroids Example implemented for [ECXX](https://gitlab.com/eliasku/ecxx) library.
 
[Original example](https://github.com/richardlord/Asteroids) source code by Richard Lord is written for his [Ash](https://github.com/richardlord/Ash) framework.

- Simple application layer is done with [SFML](https://www.sfml-dev.org/).
- [CONAN](https://conan.io/) is for C/C++ package management.
- [CMake](https://cmake.org/) to generate build targets. 

### "Easy" to Install and Run!

```bash

# install or upgrade conan
pip3 install conan --upgrade

# add [bincrafters] remote
conan remote add bincrafters -f https://api.bintray.com/conan/bincrafters/public-conan

# add [eliasku] remote
conan remote add eliasku -f https://api.bintray.com/conan/eliasku/public-conan

# clone source code
git clone https://gitlab.com/eliasku/ecxx-asteroids.git
cd ecxx-asteroids

# create build folder
mkdir -p build-debug && cd build-debug

# install dependencies
conan install -b=missing -s build_type=Debug ..

# generate build scripts
cmake -DCMAKE_BUILD_TYPE=Debug ..

# build
cmake --build . -- -j8

# run
./bin/ecxx_asteroids

```
