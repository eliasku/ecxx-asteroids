#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <ecxx/ecxx.hpp>
#include <thread>
#include "systems.h"
#include "fsm.h"
#include "config.h"

#ifdef SFML_SYSTEM_IOS
#include <SFML/Main.hpp>
#endif

std::string resources_dir() {
#ifdef SFML_SYSTEM_IOS
    return "";
#else
    return "resources/";
#endif
}

int main() {
    ecs::world w{};
    ecxx::set_world(&w);

    std::srand(static_cast<unsigned int>(std::time(NULL)));

    // Create the window of the application
    sf::ContextSettings context_settings;
    context_settings.antialiasingLevel = 1;
    sf::RenderWindow window(sf::VideoMode(game::width, game::height, 32), "ecxx : Asteroids",
                            sf::Style::Titlebar | sf::Style::Close, context_settings);
    window.setVerticalSyncEnabled(true);
    window.setFramerateLimit(60);

    sf::Clock clock;
    while (window.isOpen()) {
        // Handle events
        sf::Event event{};
        if (window.pollEvent(event)) {
            // Window closed or escape key pressed: exit
            if ((event.type == sf::Event::Closed) ||
                ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Escape))) {
                window.close();
                break;
            }
            // Window size changed, adjust view appropriately
            if (event.type == sf::Event::Resized) {
                sf::View view;
                view.setSize(game::width, game::height);
                view.setCenter(game::width / 2.f, game::height / 2.f);
                window.setView(view);
            }
        }


        const float dt = clock.restart().asSeconds();

        // Clear the window
        window.clear(sf::Color(50, 50, 100));

        update_game(dt);
        update_motion_control(dt);
        update_gun_control(dt);
        update_age(dt);
        update_movement(dt);
        update_collision();
        update_animation(dt);
        update_render_system(dt, window);
        update_fsm();

        // Display things on screen
        window.display();

        std::this_thread::sleep_for(std::chrono::milliseconds(0));
    }
    return EXIT_SUCCESS;
}