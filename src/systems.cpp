#include "systems.h"
#include "utils.h"
#include "factory.h"
#include "fsm.h"
#include "config.h"

using namespace ecxx;

void update_render_system(float dt, sf::RenderWindow& window) {
    ecs::view<display_t, position_t>().each([&window](auto& display, auto& position) {
        if (display.shape != nullptr) {
            display.shape->setPosition(
                    position.position.x,
                    position.position.y
            );
            display.shape->setRotation(position.rotation * 180.0f / M_PI);
            window.draw(*display.shape);
        }
    });
}

void update_age(float dt) {
    auto entities = ecs::rview<age_t>();
    for (auto entity : entities) {
        auto& age = ecs::get<age_t>(entity);
        age.life_remaining -= dt;
        if (age.life_remaining <= 0.0f) {
            // todo: check mofidy while iterating
            ecs::destroy(entity);
        }
    }
}

void update_animation(float dt) {
    ecs::view<animation_t>().each([&dt](animation_t& a) {
        a.animate(dt);
    });
}

void update_collision() {
    for (auto bullet_entity : ecs::rview<bullet_t, position_t, collision_t>()) {
        auto& bullet_position = ecs::get<position_t>(bullet_entity);

        for (auto asteroid_entity : ecs::rview<asteroid_t, position_t, collision_t>()) {
            auto& asteroid_position = ecs::get<position_t>(asteroid_entity);
            auto& asteroid_collision = ecs::get<collision_t>(asteroid_entity);

            if (distance(asteroid_position.position, bullet_position.position) <= asteroid_collision.radius) {
                ecs::destroy(bullet_entity);
                if (asteroid_collision.radius > 10) {
                    for (int an = 0; an < 2; ++an) {
                        auto p = asteroid_position.position + float2{rnd(-5.0f, 5.0f), rnd(-5.0f, 5.0f)};
                        auto r = asteroid_collision.radius - 10;
                        create_asteroid(r, p);
                    }
                }
                ecs::destroy(asteroid_entity);
                break;
            }
        }
    }

    for (auto spaceship_entity : ecs::view<spaceship_t, position_t, collision_t>()) {
        auto& spaceship_position = ecs::get<position_t>(spaceship_entity);
        auto& spaceship_collision = ecs::get<collision_t>(spaceship_entity);

        for (auto asteroid_entity : ecs::view<asteroid_t, position_t, collision_t>()) {
            auto& asteroid_position = ecs::get<position_t>(asteroid_entity);
            auto& asteroid_collision = ecs::get<collision_t>(asteroid_entity);

            if (distance(asteroid_position.position, spaceship_position.position) <=
                asteroid_collision.radius + spaceship_collision.radius) {
                ecs::get<fsm_t>(spaceship_entity).set_state("destroyed");
                break;
            }
        }
    }
}

void update_gun_control(float dt) {
    // do not lock `position_t`
    for (auto entity : ecs::view<gun_controls_t, gun_t>()) {

        auto& control = ecs::get<gun_controls_t>(entity);
        auto& gun = ecs::get<gun_t>(entity);
        auto& position = ecs::get<position_t>(entity);

        gun.shooting = sf::Keyboard::isKeyPressed(static_cast<sf::Keyboard::Key>(control.trigger));
        gun.time_since_last_shot += dt;
        if (gun.shooting && gun.time_since_last_shot >= gun.minimum_shot_interval) {
            create_user_bullet(gun, position);
            gun.time_since_last_shot = 0.0f;
        }
    }
}

void update_motion_control(float dt) {
    auto entities = ecs::view<motion_control_t, motion_t, position_t>();
    for (auto entity : entities) {
        auto& control = ecs::get<motion_control_t>(entity);
        auto& position = ecs::get<position_t>(entity);
        auto& motion = ecs::get<motion_t>(entity);

        auto left = sf::Keyboard::isKeyPressed(static_cast<sf::Keyboard::Key>(control.left));
        auto right = sf::Keyboard::isKeyPressed(static_cast<sf::Keyboard::Key>(control.right));
        auto accelerate = sf::Keyboard::isKeyPressed(static_cast<sf::Keyboard::Key>(control.accelerate));

        if (left) {
            position.rotation -= control.rotation_rate * dt;
        }

        if (right) {
            position.rotation += control.rotation_rate * dt;
        }

        if (accelerate) {
            motion.velocity += direction(position.rotation) * control.acceleration_rate * dt;
        }
    }
}

void update_movement(float dt) {
    auto entities = ecs::view<motion_t, position_t>();

    const float width = game::width;
    const float height = game::height;

    for (auto entity : entities) {
        auto& position = ecs::get<position_t>(entity);
        auto& motion = ecs::get<motion_t>(entity);

        position.position += motion.velocity * dt;

        if (position.position.x < 0) {
            position.position.x += width;
        }
        if (position.position.x > width) {
            position.position.x -= width;
        }
        if (position.position.y < 0) {
            position.position.y += height;
        }
        if (position.position.y > height) {
            position.position.y -= height;
        }
        position.rotation += motion.angular_velocity * dt;
        if (motion.damping > 0) {
            const float2 damp = abs(direction(position.rotation) * motion.damping * dt);
            if (motion.velocity.x > damp.x) {
                motion.velocity.x -= damp.x;
            } else if (motion.velocity.x < -damp.x) {
                motion.velocity.x += damp.x;
            } else {
                motion.velocity.x = 0.0f;
            }
            if (motion.velocity.y > damp.y) {
                motion.velocity.y -= damp.y;
            } else if (motion.velocity.y < -damp.y) {
                motion.velocity.y += damp.y;
            } else {
                motion.velocity.y = 0.0f;
            }
        }
    }
}

void update_game(float dt) {

    int game_states_count = 0;
    int spaceships_count = 0;
    int asteroids_count = 0;
    int bullets_count = 0;

    {
        auto game_states = ecs::view<game_state_t>();
        auto spaceships = ecs::view<spaceship_t, position_t>();
        auto asteroids = ecs::view<asteroid_t, position_t, collision_t>();
        auto bullets = ecs::view<bullet_t, position_t, collision_t>();

        for (auto t : game_states) {
            ++game_states_count;
        }

        for (auto t : spaceships) {
            ++spaceships_count;
        }

        for (auto t : asteroids) {
            ++asteroids_count;
        }

        for (auto t : bullets) {
            ++bullets_count;
        }
    }

    for (auto entity : ecs::view<game_state_t>()) {
        auto& game_state = ecs::get<game_state_t>(entity);
        if (spaceships_count == 0) {
            if (game_state.lives > 0) {
                float2 new_spaceship_position{
                        game::width * 0.5f,
                        game::height * 0.5f
                };
                bool clear_to_add_spaceship = true;

                for (auto asteroid_entity : ecs::view<asteroid_t, position_t, collision_t>()) {
                    auto& asteroid_position = ecs::get<position_t>(asteroid_entity);
                    auto& asteroid_collision = ecs::get<collision_t>(asteroid_entity);
                    if (distance(asteroid_position.position, new_spaceship_position) <=
                        asteroid_collision.radius + 50) {
                        clear_to_add_spaceship = false;
                        break;
                    }
                }
                if (clear_to_add_spaceship) {
                    create_spaceship();
                    game_state.lives--;
                }
            } else {
                // game over
            }
        }

        if (asteroids_count == 0 && bullets_count == 0 && spaceships_count > 0) {
            // next level
            auto spaceship_entity = *ecs::view<spaceship_t, position_t>().begin();
            auto& spaceship = ecs::get<spaceship_t>(spaceship_entity);
            auto& spaceship_position = ecs::get<position_t>(spaceship_entity);
            game_state.level++;
            int asteroid_count = 2 + game_state.level;
            for (int i = 0; i < asteroid_count; ++i) {
                // check not on top of spaceship
                float2 position;
                int iterations = 20;
                do {
                    position = {
                            rnd(0.0f, game::width),
                            rnd(0.0f, game::height)
                    };
                } while (distance(position, spaceship_position.position) <= 80.0f
                         && iterations-- > 0);
                create_asteroid(30, position);
            }
        }
    }

    if (game_states_count == 0) {
        create_game();
    }
}