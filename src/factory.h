#pragma once

#include <ecxx/ecxx.hpp>
#include "components.h"

ecs::entity create_game();

ecs::entity create_asteroid(float radius, float2 pos);

ecs::entity create_spaceship();

ecs::entity create_user_bullet(const gun_t& gun, const position_t& parent_position);
