#pragma once

#include <cstdlib>

inline static float rnd() {
    return static_cast<float>(std::rand() % 0x7FFFu) / 0x7FFFu;
}

inline static float rnd(float min, float max) {
    return min + (max - min) * rnd();
}