#pragma once

#include <cmath>

template<typename T>
struct vec2 {
    T x;
    T y;

    static const vec2<T> zero;

    inline vec2<T> operator*(T scalar) const {
        return {x * scalar, y * scalar};
    }

    inline vec2<T> operator*(const vec2<T>& v) const {
        return {x * v.x, y * v.y};
    }

    inline vec2<T> operator+(const vec2<T>& v) const {
        return {x + v.x, y + v.y};
    }

    inline vec2<T> operator-(const vec2<T>& v) const {
        return {x - v.x, y - v.y};
    }

    inline vec2<T>& operator*=(const vec2<T>& v) {
        *this = *this * v;
        return *this;
    }

    inline vec2<T>& operator+=(const vec2<T>& v) {
        *this = *this + v;
        return *this;
    }
};

template<typename T>
inline vec2<T> direction(T angle) {
    return {std::cos(angle), std::sin(angle)};
}

template<typename T>
inline vec2<T> abs(const vec2<T>& v) {
    return {std::abs(v.x), std::abs(v.y)};
}

template<typename T>
inline vec2<T> rotate(const vec2<T>& v, const vec2<T>& cs_sn) {
    return {
            cs_sn.x * v.x - cs_sn.y * v.y,
            cs_sn.y * v.x + cs_sn.x * v.y
    };
}

template<typename T>
inline vec2<T> rotate(const vec2<T>& v, T angle) {
    return rotate(v, {cos(angle), sin(angle)});
}

template<typename T>
inline const vec2<T> vec2<T>::zero{0.0f, 0.0f};

using float2 = vec2<float>;


template<typename T>
float length(const vec2<T>& v) {
    return sqrtf(v.x * v.x + v.y * v.y);
}

template<typename T>
float distance(const vec2<T>& a, const vec2<T>& b) {
    return length(a - b);
}