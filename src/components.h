#pragma once

#include "vec2.h"
#include <SFML/Graphics/Shape.hpp>


struct age_t {
    float life_remaining = 0.0f;
};

struct animation_t {
    // interface link
    void animate(float dt) {}
};

struct asteroid_t {
};

struct bullet_t {
};

struct collision_t {
    float radius = 0.0f;
};

struct display_t {
    sf::Shape* shape = nullptr;
};

struct game_state_t {
    int lives = 3;
    int level = 0;
    int points = 0;
};

struct gun_t {
    bool shooting = false;
    float2 offset_from_parent{};
    float time_since_last_shot = 0.0f;
    float minimum_shot_interval = 0.0f;
    float bullet_lifetime = 0.0f;
    float spread_angle = 0.0f;


    void setup(float2 offset,
               float minimum_shot_interval_,
               float bullet_lifetime_,
               float spread_angle_) {
        offset_from_parent = offset;
        minimum_shot_interval = minimum_shot_interval_;
        bullet_lifetime = bullet_lifetime_;
        spread_angle = spread_angle_;
    }
};

struct gun_controls_t {
    int trigger = 0;
};

struct motion_t {

    float2 velocity{0.0f, 0.0f};
    float angular_velocity = 0.0f;
    float damping = 0.0f;

    void setup(float2 velocity_, float angular_velocity_, float damping_) {
        velocity = velocity_;
        angular_velocity = angular_velocity_;
        damping = damping_;
    }
};

struct motion_control_t {
    int left;
    int right;
    int accelerate;
    float acceleration_rate;
    float rotation_rate;
};

struct position_t {
    float2 position{0.0f, 0.0f};
    float rotation = 0.0f;
};

struct spaceship_t {

};