#include "factory.h"

#include "components.h"
#include "utils.h"
#include "fsm.h"
#include <ecxx/ecxx.hpp>
#include <SFML/Graphics.hpp>

sf::Shape* create_spaceship_view() {
    auto* spr = new sf::ConvexShape();
    spr->setFillColor(sf::Color::White);
    spr->setPointCount(4);
    spr->setPoint(0, sf::Vector2f(10, 0));
    spr->setPoint(1, sf::Vector2f(-7, 7));
    spr->setPoint(2, sf::Vector2f(-4, 0));
    spr->setPoint(3, sf::Vector2f(-7, -7));
    return spr;
}

sf::Shape* create_asteroid_view(float radius) {
    float angle = 0.0f;
    std::vector<float2> vertices;
    while (angle < M_PI * 2.0f) {
        vertices.push_back(direction(angle) * rnd(0.75f, 1.0f) * radius);
        angle += rnd() * 0.5f;
    }

    auto* spr = new sf::ConvexShape();
    spr->setFillColor(sf::Color::White);
    spr->setPointCount(vertices.size());
    int i = 0;
    for (auto& v: vertices) {
        spr->setPoint(i, sf::Vector2f(v.x, v.y));
        ++i;
    }
    return spr;
}

sf::Shape* create_bullet_view() {
    float radius = 2.0f;
    auto* spr = new sf::CircleShape();
    spr->setRadius(radius);
    spr->setFillColor(sf::Color::White);
    return spr;
}

ecs::entity create_game() {
    return ecs::create<game_state_t>();
}

ecs::entity create_asteroid(float radius, float2 pos) {
    auto entity = ecs::create<asteroid_t>();
    auto& position = ecs::assign<position_t>(entity);
    auto& collision = ecs::assign<collision_t>(entity);
    auto& motion = ecs::assign<motion_t>(entity);
    auto& display = ecs::assign<display_t>(entity);

    position.position = pos;
    position.rotation = 0.0f;

    collision.radius = radius;

    motion.setup({(rnd() - 0.5f) * 4 * (50 - radius), (rnd() - 0.5f) * 4.0f * (50.f - radius)}, rnd() * 2 - 1, 0.0f);
    display.shape = create_asteroid_view(radius);

    return entity;
}

ecs::entity create_spaceship() {
    auto entity = ecs::create();
    auto& fsm = ecs::assign<fsm_t>(entity);
    auto& position = ecs::assign<position_t>(entity);
    auto& display = ecs::assign<display_t>(entity);

    fsm.add_state("playing", [](ecs::entity e) {
                      ecs::assign<spaceship_t>(e);
                      ecs::assign<motion_t>(e).setup({0, 0}, 0, 15);
                      auto& mc = ecs::assign<motion_control_t>(e);
                      mc.left = sf::Keyboard::Key::Left;
                      mc.right = sf::Keyboard::Key::Right;
                      mc.accelerate = sf::Keyboard::Key::Up;
                      mc.acceleration_rate = 100;
                      mc.rotation_rate = 3;
                      ecs::assign<gun_t>(e).setup({8, 0}, 0.08, 2, 10);
                      ecs::assign<gun_controls_t>(e).trigger = sf::Keyboard::Key::Space;
                      ecs::assign<collision_t>(e).radius = 9;
                      ecs::get<display_t>(e).shape = create_spaceship_view();
                  },
                  [](auto e) {
                      ecs::remove<spaceship_t>(e);
                      ecs::remove<motion_t>(e);
                      ecs::remove<motion_control_t>(e);
                      ecs::remove<gun_t>(e);
                      ecs::remove<gun_controls_t>(e);
                      ecs::remove<collision_t>(e);
                      //entity.template unset<display_t>();
                  });

    fsm.add_state("destroyed", [](auto e) {

                      // TODO:
//var deathAnimation = new SpaceshipDeathView();
                      ecs::assign<age_t>(e).life_remaining = 5;
// TODO:
//_animation.set(entity, deathAnimation);
//_display.get(entity).addChild(deathAnimation);
                  },
                  [](auto e) {
                      ecs::remove<age_t>(e);
                      ecs::remove<animation_t>(e);
// TODO:
//_display.get(entity).removeChildren();
                  });

    position.position = {300, 225};
    position.rotation = 0;

    fsm.set_state("playing");

    return entity;
}

ecs::entity create_user_bullet(const gun_t& gun, const position_t& parent_position) {
    float rotation = parent_position.rotation + gun.spread_angle * rnd(-0.5f, 0.5f) * M_PI / 180.0f;
    auto dir = direction(rotation);
    auto velocity = 300.0f;

    auto entity = ecs::create<bullet_t>();

    auto& position = ecs::assign<position_t>(entity);
    auto& collision = ecs::assign<collision_t>(entity);
    auto& motion = ecs::assign<motion_t>(entity);
    auto& display = ecs::assign<display_t>(entity);

    ecs::assign<age_t>(entity).life_remaining = gun.bullet_lifetime;
    position.position = rotate(gun.offset_from_parent, dir) + parent_position.position;
    position.rotation = 0.0f;
    collision.radius = 0.0f;
    motion.setup(dir * velocity, 0.0f, 0.0f);

    display.shape = create_bullet_view();

    return entity;
}