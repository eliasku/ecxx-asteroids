#pragma once

#include <utility>
#include <ecxx/ecxx.hpp>
#include <string>
#include <functional>
#include <unordered_map>

struct fsm_t {

    using fsm_callback_type = std::function<void(ecs::entity)>;

    std::string state;

    std::string next;

    std::unordered_map<std::string, fsm_callback_type> on_enter;
    std::unordered_map<std::string, fsm_callback_type> on_exit;

    void add_state(const std::string& name, fsm_callback_type enter, fsm_callback_type exit) {
        on_enter[name] = std::move(enter);
        on_exit[name] = std::move(exit);
    }

    void set_state(const std::string& name) {
        next = name;
    }
};


inline void update_fsm() {
    auto view = ecs::view<fsm_t>();

    for (auto entity : view) {
        auto& fsm = ecs::get<fsm_t>(entity);
        if (fsm.state != fsm.next) {
            auto& exitCallback = fsm.on_exit[fsm.state];
            auto& enterCallback = fsm.on_enter[fsm.next];

            if (exitCallback) {
                exitCallback(entity);
            }
            if (enterCallback) {
                enterCallback(entity);
            }
            fsm.state = fsm.next;
        }
    }
}