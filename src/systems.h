#pragma once

#include "components.h"
#include <ecxx/ecxx.hpp>
#include <SFML/Graphics.hpp>

void update_render_system(float dt, sf::RenderWindow& window);

void update_age(float dt);

void update_animation(float dt);

void update_collision();

void update_gun_control(float dt);

void update_motion_control(float dt);

void update_movement(float dt);

void update_game(float dt);